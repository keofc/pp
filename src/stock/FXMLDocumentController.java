package stock;

import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXTabPane;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class FXMLDocumentController implements Initializable {

    private JFXTabPane TabHome;
    @FXML
    private AnchorPane hwd;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    private void pay_pro(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/pay_product/pay_pro.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
    }

    @FXML
    private void exit_wd(MouseEvent event) {
        Stage stage = (Stage) hwd.getScene().getWindow();
        stage.close();
    }
}
