package Login;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class LoginController implements Initializable {

    @FXML
    private JFXTextField txtuser;
    @FXML
    private JFXPasswordField txtpass;
    @FXML
    private AnchorPane wd;
    private Connection c;
    private PreparedStatement ps;
    private ResultSet rs;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void bt_login(MouseEvent event) throws IOException {
        if (txtuser.getText().equals(getEmail()) && txtpass.getText().equals(getPassword())) {
            Stage stage = (Stage) wd.getScene().getWindow();
            stage.close();
            Parent root = FXMLLoader.load(getClass().getResource("/Loading/Loading.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            //stage.initStyle(StageStyle.UNDECORATED);
            stage.show();
        } else {
            Alert alert = new Alert(Alert.AlertType.NONE, "Invalid Username  or Password", ButtonType.OK);
            alert.setTitle("Invalid");
            alert.showAndWait();
        }
    }

    @FXML
    private void bt_exit(MouseEvent event) {
        Stage stage = (Stage) wd.getScene().getWindow();
        stage.close();
    }

    private String getEmail() {
        String email = "";
        try {
            rs = getConnection().createStatement().executeQuery("select user_code from sml_user_list  where user_code='" + txtuser.getText() + "'");
            if (rs.next()) {
                email = rs.getString(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Login.LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return email;
    }

    private String getPassword() {
        String pass = "";
        try {
            rs = getConnection().createStatement().executeQuery("select user_password from sml_user_list  where user_password='" + txtpass.getText() + "'");
            if (rs.next()) {
                pass = rs.getString(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Login.LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pass;
    }

    public static java.sql.Connection getConnection() {
        java.sql.Connection con = null;
        try {
            String HOST = "jdbc:postgresql://localhost:5432/";
            String DB_NAME = "smlerpmaindata";
            String USERNAME = "postgres";
            String PASSWORD = "Keo123456";
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(HOST + DB_NAME, USERNAME, PASSWORD);
            System.out.println("DB connected");
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("not connection");
        }
        return con;
    }
}
