package ALS;

import java.awt.Font;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class altrt {
    public static void display(String title, String messege){
        Stage window = new Stage();
        window.setTitle(title);
        window.setMinWidth(450);
        window.setMaxHeight(100);
        
        
        Label label = new Label();
        label.setText(messege);
 //       label.setFont(new Font("Phetsarath OT", Font.ITALIC, 12));
        Button btok = new Button("OK");
        btok.setOnAction(e-> window.close());
        
        VBox layout = new VBox(5);
        layout.getChildren().addAll(label, btok);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
        
    }
}
