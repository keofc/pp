/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Loading;

import com.jfoenix.controls.JFXProgressBar;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author keoit
 */
public class LoadingController implements Initializable {

    @FXML
    private AnchorPane wd;
    @FXML
    private JFXProgressBar pbar;
    @FXML
    private ProgressIndicator pin;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Task task = taskWorker(10);
        pbar.progressProperty().unbind();
        pin.progressProperty().unbind();
        pbar.progressProperty().bind(task.progressProperty());
        pin.progressProperty().bind(task.progressProperty());
        task.setOnSucceeded(e -> {
            Stage stage = (Stage) pbar.getScene().getWindow();
            stage.close();

            Parent root;
            try {
                root = FXMLLoader.load(getClass().getResource("/stock/FXMLDocument.fxml"));
                Stage stage2 = new Stage();
                Scene scene = new Scene(root);

                stage2.setScene(scene);
                // stage2.initStyle(StageStyle.UNDECORATED);
                stage2.show();
            } catch (IOException ex) {
                Logger.getLogger(LoadingController.class.getName()).log(Level.SEVERE, null, ex);
            }

        });
        Thread thead = new Thread(task);
        thead.start();
    }

    private Task taskWorker(int second) {
        return new Task() {
            @Override
            protected Object call() throws Exception {
                for (int i = 0; i < second; i++) {
                    updateProgress(i + 1, second);
                    Thread.sleep(1000);
                }
                return true;
            }
        ;
    }
;
}
}
