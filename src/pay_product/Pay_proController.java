package pay_product;

import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 *
 * @author keoit
 */
public class Pay_proController implements Initializable {

    @FXML
    private TableView<list_doc> Tb1;
    @FXML
    private TableColumn<list_doc, String> clm_doc_no;
    @FXML
    private TableColumn<list_doc, String> clDate;
    @FXML
    private TableColumn<?, ?> sl;
    @FXML
    private JFXTextField txtSch;
    @FXML
    private TableView<Product_list> tb2;
    @FXML
    private TableColumn<Product_list, Boolean> clmck;
    @FXML
    private TableColumn<?, ?> clmProcode;
    @FXML
    private TableColumn<?, ?> clmProname;
    @FXML
    private TableColumn<?, ?> clmqqty;
    @FXML
    private TableColumn<?, ?> clmunit;
    @FXML
    private TableColumn<Product_list, String> clmqty_out;
    @FXML
    private DatePicker date;
    @FXML
    private JFXTextField doc_no;
    @FXML
    private AnchorPane wds;
    Connection c;
    ResultSet rs;
    PreparedStatement ps;
    private ObservableList<list_doc> data;
    private ObservableList<Product_list> data1;
    @FXML
    private ComboBox<String> ccb;
    String sql;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        c = DBconnect.ConnectDB.getConnection();
        data = FXCollections.observableArrayList();
        data1 = FXCollections.observableArrayList();
        max();
        setCellTable();
        settotext();
        setCellTable2();
        date.setValue(LocalDate.now());
        date.setConverter(new StringConverter<LocalDate>() {
            private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

            @Override
            public String toString(LocalDate localDate) {
                if (localDate == null) {
                    return "";
                }
                return dateTimeFormatter.format(localDate);
            }

            @Override
            public LocalDate fromString(String dateString) {
                if (dateString == null || dateString.trim().isEmpty()) {
                    return null;
                }
                return LocalDate.parse(dateString, dateTimeFormatter);
            }
        });
        ccb.getItems().addAll(
                "01",
                "02",
                "03",
                "04",
                "05",
                "06",
                "07",
                "08",
                "09"
        );
        ccb.getSelectionModel().select("01");
        loadsqltotb();
        ccb.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                data.clear();
                loadsqltotb();
            }
        });
        clmqty_out.setCellFactory(TextFieldTableCell.forTableColumn());

    }

    private void setCellTable() {
        clm_doc_no.setCellValueFactory(new PropertyValueFactory<>("doc_no"));
        clDate.setCellValueFactory(new PropertyValueFactory<>("doc_date"));
    }

    private void loadsqltotb() {
        try {
            java.util.Date dates
                    = java.util.Date.from(date.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
            java.sql.Date sqlDate = new java.sql.Date(dates.getTime());
            System.out.println(ccb.getValue());
            System.out.println(sqlDate);

            rs = c.createStatement().executeQuery("SELECT doc_no,doc_date from ic_trans where  doc_no not in (select DISTINCT doc_no from stock) and doc_no in (SELECT doc_no from ic_trans_detail where trans_flag='44' and wh_code='" + ccb.getValue() + "' and doc_date='" + sqlDate + "' ) ");
            while (rs.next()) {
                data.add(new list_doc(rs.getString(1), rs.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Pay_proController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Tb1.setItems(data);
    }
    String D;

    private void settotext() {
        Tb1.setOnMouseClicked((MouseEvent e) -> {
            list_doc pl = Tb1.getItems().get(Tb1.getSelectionModel().getSelectedIndex());
            D = pl.getDoc_no();
            data1.clear();
            loadsqltotb2();
        });
    }

    @FXML
    private void save_print(MouseEvent event) {
//        java.util.Date dates
//                = java.util.Date.from(date.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
//        java.sql.Date sqlDate = new java.sql.Date(dates.getTime());
//        try {
//            sql = "INSERT INTO public.stock( stock_id, doc_no, doc_date,wh_code, status) VALUES (?, ?, ?, ?, ?)";
//            ps = c.prepareStatement(sql);
//            ps.setString(1, doc_no.getText());
//            ps.setString(2, D);
//            ps.setDate(3, sqlDate);
//            ps.setString(4, ccb.getValue());
//            ps.setString(5, "ກຳລັງເບີກສິນຄ້າ");
//            System.out.println("ຫົວບີນ ok");
//            if (ps.executeUpdate() > 0) {
//                for (Product_list li : data1) {
//                    sql = "INSERT INTO public.stock_detail(stock_id, doc_ref_no, doc_date, prod_code, pro_name, pro_qty, unit_code, pro_qty_out, status)VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
//                    ps = c.prepareStatement(sql);
//                    ps.setString(1, doc_no.getText());
//                    ps.setString(2, D);
//                    ps.setDate(3, sqlDate);
//                    ps.setString(4, li.getProduct_code());
//                    ps.setString(5, li.getProduct_name());
//                    ps.setDouble(6, Double.valueOf(li.getPro_qty()));
//                    ps.setString(7, li.getUnit_code());
//                    ps.setDouble(8, Double.valueOf(li.getPro_qty_out()));
//                    ps.setString(9, "ກຳລັງເບີກເຕັມຈຳນວນ");
//                    ps.execute();
//
//                }
//                System.out.println("Ok");
//                doc_no.setText("");
//                max();
//                data.clear();
//                loadsqltotb();
//                data1.clear();
//            }
//            System.out.println("Ok");
//        } catch (SQLException ex) {
//            Logger.getLogger(Pay_proController.class.getName()).log(Level.SEVERE, null, ex);
//        }

    }

    private void max() {
        sql = "SELECT MAX(stock_id) as stock_id from stock where stock_id like 'STK%'";
        try {
            rs = c.createStatement().executeQuery(sql);
            while (rs.next()) {
                if (Objects.isNull(rs.getString("stock_id"))) {
                    doc_no.setText("STK190001");
                } else {
                    String id = rs.getString("stock_id");
                    String[] out = id.split("STK");
                    String sd = out[1];
                    int a = 1 + Integer.valueOf(sd);
                    doc_no.setText("STK" + a);
                }
            }
        } catch (Exception e) {
        }
    }

    @FXML
    private void exit(MouseEvent event) {
        Stage stage = (Stage) wds.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void drab(MouseEvent event) {
    }

    @FXML
    private void fpro(MouseEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/aldpay/aldpay.fxml"));
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
    }

    @FXML
    private void st_se(MouseEvent event) {
    }

    private void setCellTable2() {
        clmck.setCellValueFactory(new PropertyValueFactory<>("select"));
        clmProcode.setCellValueFactory(new PropertyValueFactory<>("Product_code"));
        clmProname.setCellValueFactory(new PropertyValueFactory<>("Product_name"));
        clmqqty.setCellValueFactory(new PropertyValueFactory<>("Pro_qty"));
        clmunit.setCellValueFactory(new PropertyValueFactory<>("Unit_code"));
        clmqty_out.setCellValueFactory(new PropertyValueFactory<>("Pro_qty_out"));
    }

    private void loadsqltotb2() {
        try {
            rs = c.createStatement().executeQuery("SELECT item_code, item_name,qty,unit_code from ic_trans_detail where trans_flag='44' and doc_no='" + D + "' and wh_code='" + ccb.getValue() + "'");
            while (rs.next()) {
                data1.add(new Product_list(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(3)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Pay_proController.class.getName()).log(Level.SEVERE, null, ex);
        }
        tb2.setItems(data1);
    }

}
