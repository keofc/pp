/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pay_product;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.CheckBox;

/**
 *
 * @author keoit
 */
public class Product_list {

    private String Product_code;
    private String Product_name;
    private String Pro_qty;
    private String Unit_code;
    private String Pro_qty_out;
    private CheckBox select;

    public Product_list( String Product_code, String Product_name, String Pro_qty, String Unit_code, String Pro_qty_out ) {
        this.Product_code = Product_code;
        this.Product_name = Product_name;
        this.Pro_qty = Pro_qty;
        this.Unit_code = Unit_code;
        this.Pro_qty_out = Pro_qty_out;
                this.select = new CheckBox();
    }
    /**
     * @return the Product_code
     */
    public String getProduct_code() {
        return Product_code;
    }

    /**
     * @param Product_code the Product_code to set
     */
    public void setProduct_code(String Product_code) {
        this.Product_code = Product_code;
    }

    /**
     * @return the Product_name
     */
    public String getProduct_name() {
        return Product_name;
    }

    /**
     * @param Product_name the Product_name to set
     */
    public void setProduct_name(String Product_name) {
        this.Product_name = Product_name;
    }

    /**
     * @return the Pro_qty
     */
    public String getPro_qty() {
        return Pro_qty;
    }

    /**
     * @param Pro_qty the Pro_qty to set
     */
    public void setPro_qty(String Pro_qty) {
        this.Pro_qty = Pro_qty;
    }

    /**
     * @return the Unit_code
     */
    public String getUnit_code() {
        return Unit_code;
    }

    /**
     * @param Unit_code the Unit_code to set
     */
    public void setUnit_code(String Unit_code) {
        this.Unit_code = Unit_code;
    }

    /**
     * @return the Pro_qty_out
     */
    public String getPro_qty_out() {
        return Pro_qty_out;
    }

    /**
     * @param Pro_qty_out the Pro_qty_out to set
     */
    public void setPro_qty_out(String Pro_qty_out) {
        this.Pro_qty_out = Pro_qty_out;
    }
public  CheckBox getSelect(){
    return select;
}
public  void setSelect(CheckBox select){
    this.select = select;
}
}
