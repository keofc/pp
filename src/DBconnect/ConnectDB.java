package DBconnect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectDB {

    public static Connection getConnection() {
        Connection con = null;
        try {
            String HOST = "jdbc:postgresql://192.168.0.129:5432/";
            String DB_NAME = "pp2019";
            String USERNAME = "postgres";
            String PASSWORD = "sml";
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(HOST + DB_NAME, USERNAME, PASSWORD);
            System.out.println("DB connected");
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("not connection");
        }
        return con;
    }
}
