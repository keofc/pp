package aldpay;

public class list_doc {
    private String doc_no;
    private String doc_date;

    public list_doc(String doc_no, String doc_date) {
        this.doc_no = doc_no;
        this.doc_date = doc_date;
    }

    /**
     * @return the doc_no
     */
    public String getDoc_no() {
        return doc_no;
    }

    /**
     * @param doc_no the doc_no to set
     */
    public void setDoc_no(String doc_no) {
        this.doc_no = doc_no;
    }

    /**
     * @return the doc_date
     */
    public String getDoc_date() {
        return doc_date;
    }

    /**
     * @param doc_date the doc_date to set
     */
    public void setDoc_date(String doc_date) {
        this.doc_date = doc_date;
    }
    
}