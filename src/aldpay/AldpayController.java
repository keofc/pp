package aldpay;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pay_product.Pay_proController;

/**
 * FXML Controller class
 *
 * @author keoit
 */
public class AldpayController implements Initializable {

    @FXML
    private AnchorPane wd;
    @FXML
    private TableView<list_doc> tbshow;
    @FXML
    private TableColumn<list_doc, String> clm_doc_no;
    @FXML
    private TableColumn<list_doc, String> clm_doc_date;
    String sql;
    ResultSet rs = null;
    Connection c = null;
    PreparedStatement ps = null;
    private ObservableList<list_doc> data;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        c = DBconnect.ConnectDB.getConnection();
        data = FXCollections.observableArrayList();
        setCellTable();
        loadsqltotb();
        settotext();
    }

    private void setCellTable() {
        clm_doc_no.setCellValueFactory(new PropertyValueFactory<>("doc_no"));
        clm_doc_date.setCellValueFactory(new PropertyValueFactory<>("doc_date"));
    }

    private void settotext() {
        tbshow.setOnMouseClicked((MouseEvent e) -> {
            list_doc pl = tbshow.getItems().get(tbshow.getSelectionModel().getSelectedIndex());
            Stage stage = (Stage) wd.getScene().getWindow();
            stage.close();
        });
    }

    private void loadsqltotb() {
        try {
            sql = "SELECT stock_id, doc_no from stock ORDER by stock_id desc";
            rs = c.createStatement().executeQuery(sql);
            while (rs.next()) {
                data.add(new list_doc(rs.getString(1), rs.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Pay_proController.class.getName()).log(Level.SEVERE, null, ex);
        }
        tbshow.setItems(data);
    }
}
